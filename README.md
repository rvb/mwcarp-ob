# The Ob benchmark set

The `Ob` benchmark set (named after the [west-siberian river Ob](https://en.wikipedia.org/wiki/Ob_River)) is a set of benchmark instances for the Mixed Capacitated Arc Routing Problem that simulates sities that are divided into several components by a river.  It has been generated as described in

- [René van Bevern, Christian Komusiewicz, and Manuel Sorge (2017).  A
  parameterized approximation algorithm for the mixed and windy
  capacitated arc routing problem: theory and experiments.
  _Networks_, 70(3):262–278.](http://dx.doi.org/10.1002/net.21742)

The `ob1` directory contains cities that are split into two or three components, the `ob2` directory contains cities that are split into four or five components.  The `obify` directory is a tool to generate `Ob` instances from other available data sets.

Two example instances are rendered below:

<a href="ob-egl-g2-E.png"><img src="ob-egl-g2-E.png" width="400px"></a> <a href="ob2-Lpr-b-05.png"><img src="ob2-Lpr-b-05.png" width="400px"></a>

## Usage of the obify tool

Just download a [Zip archive](https://gitlab.com/rvb/mwcarp-ob/repository/archive.zip?ref=master) or [tar.gz archive](https://gitlab.com/rvb/mwcarp-ob/repository/archive.tar.gz?ref=master) from Github or clone the project using

```
git clone https://gitlab.com/rvb/mwcarp-ob.git
```

To get usage instructions and supported options, run

```
python obify/obify.py
```

Options allow to change the number of initial bridges (each bridge causes two river banks), the allowed deviation of a city component from the average component size, and to output a PDF drawing of the generated instance.

## File format

Instances can be given in the [classical format for the undirected Capacitated Arc Routing problem](http://logistik.bwl.uni-mainz.de/Dateien/READ_ME) or in the [format for the mixed Capacitated Arc Routing problem](http://www.uv.es/~belengue/mcarp/READ_ME), where dump consts and different consts for serving and traversing are supported.

Benchmark instances in both formats are available at <http://logistik.bwl.uni-mainz.de/benchmarks.php> and <http://www.uv.es/belengue/mcarp/>, respectively.

Acknowledgments
---------------

This benchmark set was created within project 16-31-60007,
"Fixed-parameter algorithms for NP-hard routing and scheduling problems"
of the [Russian Foundation for Basic Research](http://www.rfbr.ru).

----

René van Bevern <rvb@nsu.ru>
