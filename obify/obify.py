#!/usr/bin/env python
import networkx as nx
import argparse
import sys
import carp_loader as cl
import math
import random

class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.print_help(sys.stderr)
        self.exit(2, '\n%s: error: %s\n' % (self.prog, message))

def voronoi_diagram(D, nlist):
    vd = {}
    length = nx.all_pairs_dijkstra_path_length(D, weight='cost')
    for v in D.nodes_iter():
        vd[v] = nlist[0]
        for w in nlist:
            if length[v][w] < length[v][vd[v]]:
                vd[v] = w
    return vd

def unrequire_voronoi_boundary(D, nlist, dev):
    vd = voronoi_diagram(D, nlist)

    lobo = len(D.nodes())/(len(nlist)*dev)
    print "Total number of vertices is", len(D.nodes())
    print "Want at least", lobo, "vertices in each part of the city"
    for w in nlist:
        nodes = [v for v in D.nodes() if vd[v]==w]
        if len(nodes) < lobo:
            raise Exception("Very unbalanced instance", len(nodes))
    
    deleted = []
    for e in D.edges_iter(data = True):
        if vd[e[0]] != vd[e[1]]:
            deleted.append(e)
            D.remove_edge(*e[:2])
    num_bridges = len(nlist)*len(nlist) #min(3,int(math.ceil(float(nx.number_of_nodes(D))/40)))
    print "Number of bridges to build:", num_bridges
    bridges = random.sample(deleted, num_bridges)
    for e in bridges:
        e[2]['required'] = False
        e[2]['serve_cost'] = e[2]['cost']
        e[2]['demand'] = 0
        print "added", e
        D.add_edge(*e)
        D.add_edge(e[1], e[0], attr_dict = e[2])

    for e in D.edges_iter(data = True):
        if e[2]['required'] and (D.out_degree(e[1]) == 0 or D.in_degree(e[0]) == 0):
            d=e
            d[2]['required'] = False
            d[2]['serve_cost'] = e[2]['cost']
            d[2]['demand'] = 0
            e[2]['undirected']=True            
            D.add_edge(d[1],d[0],attr_dict=d[2])

    if not nx.is_strongly_connected(D):
        raise Exception("Not so good instance", "not strongly connected")
            
    return D

if __name__ == '__main__':
    parser = ArgumentParser(description='Turns an input instance of Mixed and Windy Capacitated Arc Routing into an Ob instance',
                                     epilog='By default, splits the input city along a randomly chosen bridge.')
    parser.add_argument('-b', '--bridges', dest='bridges', type=int,
                        default=1,
                        help='Number of initial bridges to choose.  These will join different river banks.  The default is 1.')
    parser.add_argument('-d', '--deviation', dest='dev', default=2, type=float, metavar='D', help='Allows city components to be D times smaller than the average.')
    parser.add_argument('-p', '--pdf', dest='pdf',
                        help='draw a PDF image of the created instance')
    parser.add_argument('infile', metavar='INPUT',
                        help='input instance')
    parser.add_argument('outfile', metavar='OUTPUT',
                        help='output file')
    
    args = parser.parse_args()

    D = cl.load_instance(args.infile).to_digraph()
    centers = [v for e in random.sample(D.edges(), args.bridges) for v in e]
    E = unrequire_voronoi_boundary(D, centers, args.dev)
    E.graph['name'] = "ob-" + E.graph['name']
    if args.pdf:
        cl.export_graphviz(E,args.pdf)
    cl.save_instance(E,args.outfile)
    
