import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

def export_graphviz(G,out):
    plt.clf()
    edge_colors = ['black' if e[2]['required'] else 'lightgray' for e in G.edges(data=True)]
    pos = graphviz_layout(G)
    nx.draw(G, pos, with_labels=False, node_size=1, edge_color=edge_colors)
    plt.savefig(out, format="pdf")

def null_orienteer(G, h, t):
    return (h, t)

class CarpNetwork:
    def __init__(self):
        self.name = ""
        self.req_nodes = []
        self.req_arcs = []
        self.req_edges = []
        self.normal_arcs = []
        self.normal_edges = []
        self.lobo = 0
        self.depo = 0
        self.capacity = 0
        self.dump_cost = 0

    def add_req_node(self, v, cost, demand):
        self.req_nodes.append({'head': v, 'cost': cost, 'demand': demand})

    def add_normal_edge(self, u, v, cost):
        self.normal_edges.append({'head': u, 'tail': v, 'cost': cost})
        
    def add_normal_arc(self, u, v, cost):
        self.normal_arcs.append({'head': u, 'tail': v, 'cost': cost})

    def add_req_edge(self, u, v, travel_cost, demand, serve_cost):
        self.req_edges.append({'head': u, 'tail': v, 'travel_cost': travel_cost, 'demand' : demand, 'serve_cost' : serve_cost})

    def add_req_arc(self, u, v, travel_cost, demand, serve_cost):
        self.req_arcs.append({'head': u, 'tail': v, 'travel_cost': travel_cost, 'demand' : demand, 'serve_cost' : serve_cost})

    def to_digraph_base(self):
        G = nx.DiGraph()
        G.graph['depo'] = self.depo
        G.graph['name'] = self.name
        G.graph['lobo'] = self.lobo
        G.graph['capacity'] = self.capacity
        G.graph['dump_cost'] = self.dump_cost

        for v in self.req_nodes:
            G.add_edge(v['head'], v['head'], serve_cost = v['cost'], cost = 0, demand = v['demand'], required = True, undirected = False)

        for e in self.normal_arcs:
            G.add_edge(e['head'], e['tail'], serve_cost = e['cost'], cost = e['cost'], demand = 0, required = False, undirected = False)

        for e in self.req_arcs:
            G.add_edge(e['head'], e['tail'], serve_cost = e['serve_cost'], cost = e['travel_cost'], demand = e['demand'], required = True, undirected = False)

        for e in self.normal_edges:
            G.add_edge(e['head'], e['tail'], serve_cost = e['cost'], cost = e['cost'], demand = 0, required = False, undirected = False)
            G.add_edge(e['tail'], e['head'], serve_cost = e['cost'], cost = e['cost'], demand = 0, required = False, undirected = False)

        return G

    def to_digraph(self, orienteer = null_orienteer):
        G = self.to_digraph_base()
        for e in self.req_edges:
            (h, t) = orienteer(G, e['head'], e['tail'])
            G.add_edge(h, t, serve_cost = e['serve_cost'], cost = e['travel_cost'], demand = e['demand'], required = True, undirected = True)
            G.add_edge(t, h, serve_cost = e['travel_cost'], cost = e['travel_cost'], demand = 0, required = False, undirected = True)
            
        return G

def load_nearp(filename):
    required_nodes = False
    required_arcs = False
    required_edges = False
    normal_arcs = False
    normal_edges = False

    G = CarpNetwork()
    for line in open(filename):
        if line.startswith("Name"):
            G.name = line.split(':')[1].strip()
            continue
        if line.startswith("Optimal value"):
            opt = int(line.split(':')[1])
            if opt >= 0:
                G.lobo = opt
        if line.startswith("Capacity"):
            G.capacity = int(line.split(':')[1])
            continue
        if line.startswith("Depot Node"):
            G.depo = int(line.split(':')[1])
            continue
        if line.startswith("ReN."):
            required_nodes = True
            required_arcs = False
            required_edges = False
            normal_arcs = False
            normal_edge = False
            continue
        if line.startswith("ReE."):
            required_nodes = False
            required_arcs = False
            required_edges = True
            normal_arcs = False
            normal_edge = False
            continue
        if line.startswith("EDGE"):
            required_nodes = False
            required_arcs = False
            required_edges = False
            normal_arcs = False
            normal_edge = True
            continue
        if line.startswith("ReA."):
            required_nodes = False
            required_arcs = True
            required_edges = False
            normal_arcs = False
            normal_edge = False
            continue
        if line.startswith("ARC"):
            required_nodes = False
            required_arcs = False
            required_edges = False
            normal_arcs = True
            normal_edge = False
            continue
        if required_nodes:
            parse = line.split()
            if not parse:
                continue
            v = int(parse[0].strip('N '))
            d = int(parse[1])
            c = int(parse[2])
            G.add_req_node(v, c, d)
            continue
        if required_arcs or required_edges:
            parse = line.split()
            if not parse:
                continue
            u = int(parse[1])
            v = int(parse[2])
            tc = int(parse[3])
            d = int(parse[4])
            sc = int(parse[5])
            if required_arcs: 
                G.add_req_arc(u, v, tc, d, sc)
            else:
                G.add_req_edge(u, v, tc, d, sc)
        if normal_arcs or normal_edges:
            parse = line.split()
            if not parse:
                continue
            u = int(parse[1])
            v = int(parse[2])
            tc = int(parse[3])
            if normal_arcs:
                G.add_normal_edge(u, v, tc)
            else:
                G.add_normal_arc(u, v, tc)
    return G
            
            
def load_egl(filename, minimize_imbalance = 0):
    normal_arcs = False
    required_arcs = False
    
    G = CarpNetwork()
    G.dump_cost = 0
    for line in open(filename):
        if line.startswith("CAPACIDAD"):
            G.capacity = int(line.split(':')[1])
            continue
        if line.startswith("COMENTARIO"):
            if "OPT" in line:
                G.lobo = int(line.split('=')[1])
            else:
                G.lobo = int(line.split('=')[2])
            continue
        if line.startswith("NOMBRE"):
            G.name = line.split(':')[1].strip()
            continue
        if line.startswith("LISTA_ARISTAS_REQ"):
            required_arcs = True
            normal_arcs = False
            continue
        if line.startswith("DEPOSITO"):
            G.depo = int(line.split(':')[1])
            continue
        if line.startswith("LISTA_ARISTAS_NOREQ"):
            required_arcs = False
            normal_arcs = True
            continue
        if required_arcs:
            parse = line.split()
            u = int(parse[1].strip('), '))
            v = int(parse[2].strip('), '))
            tc = int(parse[4].strip('), '))
            d = int(parse[6].strip('), '))
            G.add_req_edge(u, v, tc, d, tc)
        if normal_arcs:
            parse = line.split()
            u = int(parse[1].strip('), '))
            v = int(parse[2].strip('), '))
            tc = int(parse[4].strip('), '))
            G.add_normal_edge(u, v, tc)
    return G

def load_mval_lpr(filename):
    normal_arcs = False
    required_arcs = False
    normal_edges = False
    required_edges = False
    
    G = CarpNetwork()
    for line in open(filename):
        if line.startswith("CAPACITY"):
            G.capacity = int(line.split(':')[1])
            continue
        if line.startswith("LOBO"):
            G.lobo = int(line.split(':')[1])
            continue
        if line.startswith("NAME"):
            G.name = line.split(':')[1].strip()
            continue
        if line.startswith("DUMPING_COST"):
            G.dump_cost = int(line.split(':')[1])
        if line.startswith("LIST_REQ_ARCS"):
            required_arcs = True
            normal_arcs = False
            normal_edges = False
            required_edges = False
            continue
        if line.startswith("DEPOT"):
            G.depo = int(line.split(':')[1])
            continue
        if line.startswith("LIST_NOREQ_ARCS"):
            required_arcs = False
            normal_arcs = True
            required_edges = False
            normal_edges = False
            continue
        if line.startswith("LIST_REQ_EDGES"):
            required_arcs = False
            normal_arcs = False
            required_edges = True
            normal_edges = False
            continue
        if line.startswith("LIST_NOREQ_EDGES"):
            normal_edges = True
            normal_arcs = False
            required_edges = False
            required_arcs = False
            continue
        if required_arcs or required_edges:
            parse = line.split()
            read_arc = parse[0].strip('()').split(',')
            u = int(read_arc[0])
            v = int(read_arc[1])
            sc = int(parse[2].strip('), '))
            tc = int(parse[4].strip('), '))
            d = int(parse[6].strip('), '))
            # if we are reading an edge, we are allowed to travel back,
            # but require only one direction to be served.
            if required_arcs: 
                G.add_req_arc(u, v, tc, d, sc)
            else:
                G.add_req_edge(u, v, tc, d, sc)
        if normal_arcs or normal_edges:
            parse = line.split()
            read_arc = parse[0].strip('()').split(',')
            u = int(read_arc[0])
            v = int(read_arc[1])
            tc = int(parse[2].strip('), '))
            if normal_arcs:
                G.add_normal_arc(u, v, tc)
            else:
                G.add_normal_edge(u, v, tc)
    return G

def load_instance(f):
    for line in open(f):
        if line.startswith("NOMBRE"):
            return load_egl(f)
        if line.startswith("ReN."):
            return load_nearp(f)
        if "serv_cost" in line:
            return load_mval_lpr(f)

def save_instance(D,out):
    all_edges = D.edges(data=True)
    req_edges = [e for e in all_edges if e[2]['required']]
    nonreq_edges = [e for e in all_edges if not e[2]['required']]
    req_directed = [e for e in req_edges if not e[2]['undirected']]
    req_undirected = [e for e in req_edges if e[2]['undirected']]
    saved_edge = {}
    
    with open(out, "w") as f:
        print >> f, "NAME :", out
        print >> f, "NODES :", nx.number_of_nodes(D)
        print >> f, "REQ_EDGES :", len(req_undirected)
        print >> f, "NOREQ_EDGES :", 0
        print >> f, "REQ_ARCS :", len(req_directed)
        print >> f, "NOREQ_ARCS :", len(nonreq_edges)
        print >> f, "CAPACITY :", D.graph['capacity']
        print >> f, "DUMPING_COST :", D.graph['dump_cost']
        print >> f, "LIST_REQ_EDGES :"

        for e in req_undirected:
            if (e[0],e[1]) not in saved_edge:
                print >> f, "(%d,%d) serv_cost %d trav_cost %d demand %d" % (e[0], e[1], e[2]['serve_cost'], e[2]['cost'], e[2]['demand'])
                saved_edge[(e[0],e[1])] = True
                saved_edge[(e[1],e[0])] = True

        print >> f, "LIST_REQ_ARCS :"
        for e in req_directed:
            if (e[0],e[1]) not in saved_edge:
                print >> f, "(%d,%d) serv_cost %d trav_cost %d demand %d" % (e[0], e[1], e[2]['serve_cost'], e[2]['cost'], e[2]['demand'])
                saved_edge[(e[0],e[1])] = True

        print >> f, "LIST_NOREQ_ARCS :"
        for e in nonreq_edges:
            if (e[0],e[1]) not in saved_edge:
                print >> f, "(%d,%d) cost %d" % (e[0], e[1], e[2]['cost'])
                saved_edge[(e[0],e[1])] = True
        
        print >> f, "DEPOT :", D.graph['depo']
        print >> f, "LOBO :", 1
